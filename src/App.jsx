import React, { useEffect, useState, useCallback } from 'react';
import axios from 'axios';
import Markdown from 'markdown-it';
import ReactMarkdown from 'react-markdown';
import Highlight from 'react-highlight'
import localForage from 'localforage';

import { getQuestions, optionClassName } from './utils';

import 'highlight.js/styles/atom-one-dark.css';
import './App.scss';

const QUESTIONS = 'questions';
const CURRENT = 'currentQuestion';
const ANSWERS = 'answers';
const CORRECT_ANSWERS = 'answers/correct';
const WRONG_ANSWERS = 'answers/wrong';

function App() {
  const [questions, setQuestions] = useState([]);
  const [userAnswer, setUserAnswer] = useState(null);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [correctAnsererdQuestions, setCorrectAnsererdQuestions] = useState([]);
  const [wrongAnsererdQuestions, setWrongAnsererdQuestions] = useState([]);
  // const [stats, setStats] = useState({ correct: 0, wrong: 0 });

  const q = questions[currentQuestion] || null;

  useEffect(() => {
    (async () => {
      // get questions from store
      const questionsFromCash = await localForage.getItem(QUESTIONS);
      const currentQuestionFromCash = await localForage.getItem(CURRENT);
      // const userAnswersFromCash = await localForage.getItem(ANSWERS);
      const correctAnswers = await localForage.getItem(CORRECT_ANSWERS);
      const wrongAnswers = await localForage.getItem(WRONG_ANSWERS);

      // console.log('eefffected', questionsFromCash);

      setCorrectAnsererdQuestions(correctAnswers || []);
      setWrongAnsererdQuestions(wrongAnswers || [])

      if (questionsFromCash && questionsFromCash.length) {
        setQuestions(questionsFromCash);
        setCurrentQuestion(currentQuestionFromCash || 0);
      }

      const { data } = await axios.get('https://raw.githubusercontent.com/lydiahallie/javascript-questions/master/README.md');
      const parsed = (new Markdown()).parse(data);
      const formattedQuestions = getQuestions(parsed);
      setQuestions(formattedQuestions);

      // // cash questions
      localForage.setItem(QUESTIONS, formattedQuestions);
    })();
  }, []);

  useEffect(() => () => {
    // localForage.setItem('state', {
    //   questions,
    //   currentQuestion,
    //   wrongAnsererdQuestions,
    //   correctAnsererdQuestions,
    // });
    // alert(JSON.stringify({
    //   // questions,
    //   currentQuestion,
    //   // wrongAnsererdQuestions,
    //   // correctAnsererdQuestions,
    // }));
  }, [currentQuestion])

  // console.log('ques', questions);


  const onNextQuestion = useCallback(() => {
    const nextQuestion = (currentQuestion + 1) % questions.length;
    setCurrentQuestion(nextQuestion);
    setUserAnswer(null);
    // localForage.setItem(CURRENT, nextQuestion);
  }, [currentQuestion, questions.length]);

  const onAnswerClick = useCallback((i) => {
    setUserAnswer(i);
    const nextQuestion = (currentQuestion + 1) % questions.length;

    if (i === q.answerIndex) {
      onNextQuestion();
      setCorrectAnsererdQuestions(questions => {
        const newCorrectAnsweredQuestions = [...questions, q];
        localForage.setItem(CORRECT_ANSWERS, newCorrectAnsweredQuestions);
        localForage.setItem(CURRENT, nextQuestion);
        return newCorrectAnsweredQuestions;
      });
    } else {
      setWrongAnsererdQuestions(questions => {
        const newWrongAnsweredQuestions = [...questions, q];
        localForage.setItem(WRONG_ANSWERS, newWrongAnsweredQuestions);
        localForage.setItem(CURRENT, nextQuestion);
        return newWrongAnsweredQuestions;
      });
    }
  }, [currentQuestion, onNextQuestion, q, questions.length])


  return (
    <div className="App">
      {
        q !== null && (
          <div className="question-group">
            <div className="question-stats-group">
              <div className="question-stats">
                <span className="question-stats--correct">Correct: {correctAnsererdQuestions.length}</span>
                <span>Wrong: {wrongAnsererdQuestions.length}</span>
              </div>
              <div className="question-number">Question: {currentQuestion + 1} of {questions.length}</div>
            </div>
            <div className="question-body">
              <div><h3>{q.title}</h3></div>
              {q.code && (
                <div>
                  <Highlight className="javascript">{q.code}</Highlight>
                </div>
              )}
              <div className="options">
                {
                  q.options.map((o, i) => (
                    <div
                      key={o}
                      className={optionClassName(userAnswer, i, q.answerIndex)}
                      onClick={userAnswer === null ? () => onAnswerClick(i) : null}
                    >
                      <ReactMarkdown source={o} />
                    </div>
                  ))
                }
              </div>
              {
                userAnswer !== null && (
                  <div className="question-answer--group">
                    <h4>Answer:</h4>
                    <div className="description">
                      {
                        q.description.map(o => (
                          <ReactMarkdown key={o} source={o} />
                        ))
                      }
                    </div>
                  </div>
                )
              }
              <div className="btn-group">
                <button className="next-btn" onClick={onNextQuestion}>next</button>
              </div>
            </div>
          </div>
        )
      }
    </div>
  );
}

export default App;
